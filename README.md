# THIS PIECE OF SOFTWARE IS DEPRECATED

The shit was made in 2016, is cringy as hell, and probably doesn't work. For fuck's sake, don't try to use it, even under threat of torture or death

---

# BBrowser
![Big Eye](https://github.com/Lupshenko/BBrowser/raw/master/images/logo.png)

## Features
* Standard features (bookmarks, history, incognito mode, settings, url bar with search)
* Material Design UI
* Portability (config saves in folder with browser)
* Simplicity for modification
* Plugins support
* No tabs :(

## Hotkeys
* F5 - refresh
* F12 - standard nwjs development tools
* Ctrl + Home - open the home page
* Ctrl + Left/Right arrows - go back/forward
* Ctrl + -/+, Ctrl + mouse wheel - zoom
