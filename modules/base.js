/*
 * BBrowser base module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

//Variables
	var 
		//Secondary variables
			fs = require("fs"),
			win = nw.Window.get(),
		
		//Main block
			app = {
				//Secondary variables
					isYourMomWhore: true,
				
				//Objects and init method
					init: function() {
						//Add links to elements
							this.webView.element = document.getElementById("curWebView");
							this.webView.urlBarElement = document.getElementById("urlBar");
							this.shell.navDrawer.element = document.getElementsByClassName("navDrawer")[0];
							this.shell.dialogs.uiMaskElement = document.getElementById("uiMask");
							this.settings.formElements = document.forms.settingsForm.elements;
							this.bookmarks.element = document.getElementById("Bookmarks").getElementsByClassName("content")[0];
							this.history.element = document.getElementById("History").getElementsByClassName("content")[0];
							this.plugins.element = document.getElementById("Plugins").getElementsByClassName("content")[0];
						
						//Other
							this.settings.config.read();
							this.settings.update();
							this.plugins.get();
							this.history.get();
							this.bookmarks.get();
							this.webView.goHome();
							
							win.on("new-win-policy", function(frame, url, policy) {
								if (url != window.top) {
									policy.forceCurrent();
								}
							});
					},
					
					shell: {
						window: {							
							minimize: function() {
								win.minimize();
							},
							
							changeState: function() {
								if (chrome.app.window.current().isMaximized()) {
									win.unmaximize();
								} else {
									win.maximize();
								}
							},
							
							close: function() {
								win.close();
							}
						},
						
						navDrawer: {
							//Secondary variables
								element: undefined,
								
								isVisible: false,
							
							//Methods
								turn: function() {
									if (this.isVisible) {
										this.hide();
									} else {
										this.show();
									}
								},
								
								show: function() {
									this.element.style.left = "0";
									this.isVisible = true;
								},
								
								hide: function() {
									this.element.style.left = "-50vw";
									this.isVisible = false;
								}
						},
						
						dialogs: {
							//Secondary variables
								uiMaskElement: undefined,
							
							//Methods
								show: function(name) {
									this.uiMaskElement.style.top = "0";
									document.getElementById(name).style.top = "15vh";
								},
								
								hide: function(name) {
									this.uiMaskElement.style.top = "-500vh";
									document.getElementById(name).style.top = "-500vh";
								}
						},
							
						hotkeys: {
							//Secondary variables
								isCtrlDown: false,
							
							//Methods
								handle: function(e) {
									switch (e.keyCode) {
										case 116: //F5
											app.webView.refresh();
											
											break;
										case 17:
											this.isCtrlDown = true;
											
											break;
										default:
											if (this.isCtrlDown) {
												switch (e.keyCode) {
													case 36: //Home
														app.webView.goHome();
														
														break;
													case 37: //Left arrow
														app.webView.goBack();
														
														break;
													case 39: //Right arrow
														app.webView.goForward();
														
														break;
													case 189: //Minus
														app.webView.zoom(false);
														
														break;
													case 187:	//Plus
														app.webView.zoom(true);
														
														break;
												}
											}
											
											break;
									}
								},
								
								detectUppedCtrl: function(e) {
									if (e.keyCode == 17) { //Ctrl
										this.isCtrlDown = false;
									}
								}
						}
					},
					
					webView: {
						//Secondary variables
							element: undefined,
							urlBarElement: undefined,
							
							curPageZoom: 1,
						
						//Methods
							go: function() {
								if (
									this.urlBarElement.value.indexOf("://") + 1 ||
									this.urlBarElement.value.indexOf("javascript:") == 0 ||
									this.urlBarElement.value.indexOf("data:") == 0
									
								) {
									this.element.contentWindow.location = this.urlBarElement.value;
								} else if (this.urlBarElement.value.indexOf(".") + 1 && !(this.urlBarElement.value.indexOf(" ") + 1)) {
									this.element.contentWindow.location = "http://" + this.urlBarElement.value;
								} else {
									this.element.contentWindow.location = app.settings.list.main.search + "?q=" + this.urlBarElement.value;
								}
							},
							
							refresh: function() {
								this.element.contentWindow.location.reload(true);
							},
							
							goBack: function() {
								this.element.contentWindow.history.back();
							},
							
							goForward: function() {
								this.element.contentWindow.history.forward();
							},
							
							goHome: function() {
								this.element.contentWindow.location = app.settings.list.main.homePage;
							},
							
							zoom: function(isZoomIn, e) {
								if (app.shell.hotkeys.isCtrlDown) {
									if (isZoomIn) {
										this.curPageZoom += 0.05;
										
										this.element.contentWindow.document.body.style.zoom = this.curPageZoom;
									} else {
										if (this.curPageZoom > 0) {
											this.curPageZoom -= 0.05;
											
											this.element.contentWindow.document.body.style.zoom = this.curPageZoom;
										}
									}
									
									if (e) {
										e.preventDefault();
									}
								}
							},
							
							updateInfo: function() {
								//Url
									this.urlBarElement.value = this.element.contentWindow.location;
								
								//History
									if (!app.history.isIncognitoModeActivated) {
										app.history.add();
									}
								
								//Check invalid page
									if (
										this.element.contentWindow.location == "data:text/html,chromewebdata" ||
										this.element.contentWindow.location == "about:blank"
									) {
										this.element.contentWindow.location = app.settings.list.main.fourZeroFourPage;
									}
							}
					}
			};
