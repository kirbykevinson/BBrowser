/*
 * BBrowser plugins module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

app.plugins = {
	//Secondary variables
		element: undefined,
		itemPattern: "<button class=\"button flat\">$data</button>",
		
		path: "plugins/",
		manifestsName: "plugin.json",
	
	//Methods
		get: function() {
			//Find folders with plugins
				var
					folderContent = [],
					folders = [],
					validFolders = [];
					
				try {
					folderContent = fs.readdirSync(app.plugins.path);
				} catch(e) {
					alert(e);
				}
				
				for (var i = 0; i < folderContent.length; i++) {
					try {
						if (fs.statSync(
							this.path +
							folderContent[i]
						).isDirectory()) {
							folders.push(folderContent[i]);
						}
					} catch(e) {
						alert(e);
					}
				}
				
				for (var i = 0; i < folders.length; i++) {
					if (fs.existsSync(
						this.path +
						folders[i] + "/" +
						app.plugins.manifestsName
					)) {
						validFolders.push(folders[i]);
					}
				}
				
			//Read manifests
				for (var i = 0; i < validFolders.length; i++) {
					try {
						var manifest = JSON.parse(fs.readFileSync(
							this.path +
							validFolders[i] + "/" +
							app.plugins.manifestsName,
						"utf-8"));
						
						app.settings.list.other.plugins.push({
							manifest: manifest,
							folder: validFolders[i],
							
							scripts: {},
							styles: {}
						});
					} catch(e) {
						alert(e);
					}
				}
			
			for (var i = 0; i < app.settings.list.other.plugins.length; i++) {
				var plugin = app.settings.list.other.plugins[i];
				
				//Init plugins
					//Scripts
						plugin.scripts.plugin = plugin;
						
						for (var j = 0; j < plugin.manifest.scripts.length; j++) {
							try {
								plugin.scripts[plugin.manifest.scripts[j]] = new Function("",
									fs.readFileSync(
										this.path +
										plugin.folder + "/" +
										plugin.manifest.scripts[j],
									"utf-8")
								);
								
								plugin.scripts[plugin.manifest.scripts[j]]();
							} catch(e) {
								alert(e);
							}
						}
					
					//Styles
						for (var j = 0; j < plugin.manifest.styles.length; j++) {
							try {
								plugin.styles[plugin.manifest.styles[j]] = new Function("",
									"var element = document.createElement(\"style\");" +
									"element.innerHTML = " + JSON.stringify(fs.readFileSync(
										this.path +
										plugin.folder + "/" +
										plugin.manifest.styles[j],
									"utf-8")) + ";" +
									"document.head.appendChild(element);"
								);
								
								plugin.styles[plugin.manifest.styles[j]]();
							} catch(e) {
								alert(e);
							}
						}
				
				//Add block to dialog
					this.element.innerHTML = this.itemPattern.replace("$data",
							plugin.manifest.name + " " +
							plugin.manifest.version + "<br>" +
							"By " + plugin.manifest.author + "<br>" +
							plugin.manifest.description
						)
						
						+ this.element.innerHTML;
			}
		}
};
