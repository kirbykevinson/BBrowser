/*
 * BBrowser settings module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

app.settings = {
	//Secondary variables
		formElements: undefined,
		
		list: {},
		
		defaultList: {
			main: { //In main.json
				homePage: "pages/home.html",
				fourZeroFourPage: "pages/404.html",
				search: "http://google.com/"
			},
			
			other: { //In other files
				bookmarks: [],
				history: [],
				plugins: []
			}
		},
	
	//Methods
		apply: function() {
			for (var i = 0; i < this.formElements.length; i++) {
				this.list.main[this.formElements[i].name] =
					this.parse(
						this.formElements[i].name,
						this.formElements[i].value
					);
			}
			
			this.update();
			this.config.write("main");
		},
		
		update: function() {
			for (var i = 0; i < this.formElements.length; i++) {
				this.formElements[i].value = this.list.main[this.formElements[i].name];
			}
		},
		
		parse: function(parameter, value) {
			if (value == "" || value == this.defaultList.main[parameter]) {
				return this.defaultList.main[parameter];
			}
			
			switch (parameter) {
				case "url":
					if (!(value.indexOf("://") + 1)) {
						value = "http://" + value;
						
						return value;
					}
				case "homePage":
					return value;
				case "search":
					if (!(value.indexOf("://") + 1)) {
						value = "http://" + value;
					}
					
					if (value[value.length - 1] != "/") {
						value += "/";
					}
					
					return value;
			}
		},
	
	//Config manager
		config: {
			//Secondary variables
				pathPrefix: "config/",
				
				paths: {
					main: "main.json",
					
					other: {
						bookmarks: "bookmarks.json",
						history: "history.json"
					}
				},
			
			//Methods
				read: function() {
					Object.assign(app.settings.list, app.settings.defaultList);
					
					//Read main
						var mainConfig = "";
						try {
							mainConfig = fs.readFileSync(
								this.pathPrefix +
								this.paths.main,
							"utf-8");
						} catch(e) {}
						
						if (mainConfig) {
							try {
								app.settings.list.main = JSON.parse(mainConfig);
							} catch(e) {
								alert(e);
							}
						}
					
					//Read other
						for (var i in app.settings.list.other) {
							var curOtherConfig = "";
							try {
								curOtherConfig = fs.readFileSync(
									this.pathPrefix +
									this.paths.other[i],
								"utf-8");
							} catch(e) {}
							
							if (curOtherConfig) {
								try {
									app.settings.list.other[i] = JSON.parse(curOtherConfig);
								} catch(e) {
									alert(e);
								}
							}
						}
				},
				
				write: function(mode, parameter) {
					if (mode == "main") {
						try {
							fs.writeFileSync(
								this.pathPrefix +
								this.paths.main,
								
								JSON.stringify(app.settings.list.main)
							);
						} catch(e) {
							alert(e);
						}
					} else if (mode == "other") {
						try {
							fs.writeFileSync(
								this.pathPrefix +
								this.paths.other[parameter],
								
								JSON.stringify(app.settings.list.other[parameter])
							);
						} catch(e) {
							alert(e);
						}
					}
				}
		}
};
